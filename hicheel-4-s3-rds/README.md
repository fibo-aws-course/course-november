# Даалгавар 4 - S3 + RDS

## Ажил 1 - RDS + Website

1.  MySQL or MariaDB instance public network дээр үүсгэнэ.
2.  Өөрийн PC-ээс хандаж эхлэл Database болон Table row үүсгэнэ.
3.  Түүнийгээ Snapshot авч хадгалаад түүнээсээ дамжуулж Private Zone-д шинэ DB instance үүсгэнэ.
4.  Холбогдох port-уудыг зөвхөн 1 instance-н байгаа security group-г зөвшөөрнө.
5.  Web server дээрээ Database-с мөр уншиж дэлгэцлэх веб асаана. Жишээ кодыг оруулав. (Dockerfile болон PHP код)
6.  Database instance дээр automatic snapshot болон бусад анги дээр туршсан тохиргоонуудыг туршиж үзэх.
    
    Docker веб 8081-р порт дээр асаах комманд

		  docker run -p 8081:80 --env DB_HOST={db-endpoint} --env DB_USER={db-admin} --env DB_PASS={db-pass} --env DB_NAME={db-name} --env TABLE={table-name} {image-name}



## Ажил 2 - S3 дээр статик вебсайт байршуулах

https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteHosting.html
