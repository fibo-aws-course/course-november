# Даалгавар 7 - Route 53

Бичлэг - https://youtu.be/CNosC_Fvvfg

## Ажил 1 - Domain холбох
freenom.com эсвэл өөр үнэгүй domain зардаг газраас домэйн авч Route 53 дээр оруулж ирэх.
## Ажил 2 - Routing policy-ууд турших.
2 өөр region дээр 2 тусдаа сервер үүсгэж 80 порт дээр вебсайт асаах.
Доорх routing policy-уудыг турших

- Simple routing
- Weighted routing
- Failover routing*
- Geolocation routing
- Multi-answer routing*

Ингэхдээ Health check үүсгэх тохиолдол (*) байна шүү.